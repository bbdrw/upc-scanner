
var JSONObject = {
   
}

var JSONObjectGame = {
   
}

var sortedArray = [];


function barcodescanner (){
    try{
	cordova.plugins.barcodeScanner.scan(
      function (result) {

            if(localStorage.getItem("action") == "checkUPC"){
                //localStorage.setItem("UPCCheck", result.text);
                $("#UPCCheck").val(result.text);
            
               $("#UPC").val(result.text);
            }
            else{
                $("#UPC").val(result.text);
            }
         
        //   alert("We got a barcode\n" +
        //         "Result: " + result.text + "\n" +
        //         "Format: " + result.format + "\n" +
        //         "Cancelled: " + result.cancelled);
      },
      function (error) {
          alert("Scanning failed: " + error);
      },
      {
          preferFrontCamera : false, // iOS and Android
          showFlipCameraButton : true, // iOS and Android
          showTorchButton : true, // iOS and Android
          torchOn: false, // Android, launch with the torch switched on (if available)
          saveHistory: true, // Android, save scan history (default false)
          prompt : "Place a barcode inside the scan area", // Android
          resultDisplayDuration: 500, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
          //formats : "QR_CODE,PDF_417", // default: all but PDF_417 and RSS_EXPANDED
          orientation : "portrait", // Android only (portrait|landscape), default unset so it rotates with the device
          disableAnimations : true, // iOS
          disableSuccessBeep: false // iOS and Android
      }
   );
    }catch(e){
        alert(e);
    }
}

var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');

        //localStorage.clear();

        if(localStorage.getItem("JSONObject") == null || localStorage.getItem("JSONObject") == ""){
            console.log("creating localstorage item \"JSONObject\"");
            localStorage.setItem("JSONObject", "");
        }
        else{
            console.log("loading JSONObject Movies");
          JSONObject = JSON.parse(localStorage.getItem("JSONObject"));
        }

        if(localStorage.getItem("JSONObjectGame") == null || localStorage.getItem("JSONObjectGame") == ""){
            console.log("creating localstorage item \"JSONObjectGame\"");
            localStorage.setItem("JSONObjectGame", "");
        }
        else{
            console.log("loading JSONObject Games");
          JSONObjectGame = JSON.parse(localStorage.getItem("JSONObjectGame"));
        }
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
        app.getUPC();
    },
    getUPC: function(){
        localStorage.setItem("action", "addUPC");
        try{
        $("#addItemBttn").on("click", function(){
            resetShowItemsDiv();
            $("#UPC").val("");
            $("#productUPC").html("");
            $("#ProductType").html("");
            $("#productTitle").html("");
            $("#ProductKind").html("");
            //barcodescanner();
            resetScanUPCForm();
            displayAddItemForm();
            
        })

        $("#scanUPC").on("click", function(){
            //$("#UPC").val("014633356793");
            barcodescanner();
        })


        $("#readFile").on("click", function(){
            app.readInfo();
        })
        }catch(e){
            alert(e);
        }

        $("#findUPC").on("click", function(){
            //app.findUPC();
            resetShowItemsDiv();
            $("#jsonObjectText").html("");
            $("#productUPC").html("");
            $("#ProductType").html("");
            $("#productTitle").html("");
            $("#ProductKind").html("");
            resetAllAddFormItems()
            $("#addItemDiv").hide();
            $("#checkForUPCDiv").show();
        })

        $(".productTypeListUPCCheck").change(function(){

            var selectedProduct = $(this).children("option:selected").val();
            console.log(selectedProduct);
            if(selectedProduct == "Movie"){
                $("#getUPCForCheck").show();
                localStorage.setItem("selectedProduct", "Movie")
            }
            else if(selectedProduct == "Games"){
                $("#getUPCForCheck").show();
                localStorage.setItem("selectedProduct", "Game")
            }
            else{
                $("#getUPCForCheck").hide();
            }
    
        });

        $("#scanCheckUPC").on("click", function(){
            localStorage.setItem("action", "checkUPC");
            //$("#UPCCheck").val("014633356793");
            barcodescanner();
           
            
        })

        $("#searchUPCButton").on("click", function(){
            

             //$("#textCheckUPC").val(localStorage.getItem("UPCCheck"));
             $("#jsonObjectText").html("");
             app.findUPC();
        });

        $(".productTypeList").change(function(){
            var selectedProduct = $(this).children("option:selected").val();
            console.log(selectedProduct);
            if(selectedProduct == "Movie"){
                $("#movieType").show();
                $("#gameType").hide();
    
                localStorage.setItem("selectedProduct", "Movie")
                
                
            }
            else if(selectedProduct == "Games"){
                $("#movieType").hide();
                $("#gameType").show();
                localStorage.setItem("selectedProduct", "Game")
            }
            else{
                $("#movieType").hide();
                $("#gameType").hide();
            }
    
        });
    
        $("#submitNewItem").on("click", function(){
            $("#jsonObjectText").html("");
            $("#productUPC").html("");
            $("#ProductType").html("");
            $("#productTitle").html("");
            $("#ProductKind").html("");
    
            var upcString = $("#UPC").val();
            var productTypeString = $(".productTypeList").children("option:selected").val();
            var productNameString = $("#title").val();
            var productAttrString;
            
            //var selectedProduct = localStorage.getItem("selectedProduct")
             
            if(localStorage.getItem("selectedProduct") == "Movie"){
                productAttrString = $("#moveTypeList").children("option:selected").val();
            }else if(localStorage.getItem("selectedProduct") == "Game"){
                productAttrString = $("#gameTypeList").children("option:selected").val();
            }
            
            if(upcString == "" || productTypeString == "" || productNameString == "" || productAttrString == ""){
                $("#jsonObjectText").html("YOU MUST FILL OUT ALL OF THE INFORMATION TO ADD!");
            }
            else{
                $("#jsonObjectText").html("");
                $("#productUPC").html("");
                $("#ProductType").html("");
                $("#productTitle").html("");
                $("#ProductKind").html("");
                var isFound = checkForDuplicate($("#UPC").val());
                if(isFound == false){
                    $("#jsonObjectText").html("");
                    saveNewEntry();
                }
                else{
    
                    var myArray =JSON.parse(localStorage.getItem("product_info"));
    
                    console.log("The upc is:" +  myArray[0]);
    
                    $("#productUPC").html("UPC: " + myArray[0]);
                    $("#ProductType").html("TYPE OF PRODUCT: : " + myArray[1].toUpperCase());
                    $("#productTitle").html("TITLE: " + myArray[2]);
                    $("#ProductKind").html("PRODUCT SPEC: " + myArray[3]);
    
                    //resetAddItemForm();
                    $("#jsonObjectText").html("YOU ALREADY OWN THIS PRODUCT!");
    
                    resetAddItemForm();
                }
            }
        });

        $("#showProduct").on("click", function(){
            // sortData();
            resetShowItemsDiv();
            $("#jsonObjectText").html("");
            $("#productUPC").html("");
            $("#ProductType").html("");
            $("#productTitle").html("");
            $("#ProductKind").html("");
            $("#UPC").val("");
            $("#productUPC").html("");
            $("#ProductType").html("");
            $("#productTitle").html("");
            $("#ProductKind").html("");
            resetAllAddFormItems()
            $("#addItemDiv").hide();
            $("#checkForUPCDiv").show();
            //barcodescanner();
            resetScanUPCForm();
            resetShowItemsDiv();
            $("#showItemTable").html("");
            sortedArray = [];
            $("#showItemsDiv").show();
        });

        $(".showItemsList").change(function(){

            var selectedProduct = $(this).children("option:selected").val();
            console.log(selectedProduct);
            if(selectedProduct == "Movie"){
                $("#showMovieType").show();
                $("#showgameType").hide();
                localStorage.setItem("selectedProduct", "Movie")
            }
            else if(selectedProduct == "Games"){
                $("#showgameType").show();
                $("#showMovieType").hide();
                localStorage.setItem("selectedProduct", "Game")
            }
    
        });

        $("#showItemsButton").on("click", function(){
            $("#showItemTable").html("");
            sortedArray = [];
            getItemsData();
        });
        
    },
    findUPC: function(){
        
        var isFound = checkForDuplicate($("#UPCCheck").val());

        $("#UPCCheck").val("");

        if(isFound == false){
            $("#jsonObjectText").html("YOU DO NOT OWN THIS YET, UPC NOT FOUND!");
        }
        else{
            var myArray =JSON.parse(localStorage.getItem("product_info"));

            console.log("The upc is:" +  myArray[0]);

            $("#productUPC").html("UPC: " + myArray[0]);
            $("#ProductType").html("TYPE OF PRODUCT: : " + myArray[1].toUpperCase());
            $("#productTitle").html("TITLE: " + myArray[2]);
            $("#ProductKind").html("PRODUCT SPEC: " + myArray[3]);

            //resetAddItemForm();
            $("#jsonObjectText").html("YOU ALREADY OWN THIS PRODUCT!");
        }
    }
};

function displayAddItemForm(){
    $("#addItemDiv").show();

    // $(".productTypeList").change(function(){
    //     var selectedProduct = $(this).children("option:selected").val();
    //     console.log(selectedProduct);
    //     if(selectedProduct == "Movie"){
    //         $("#movieType").show();
    //         $("#gameType").hide();

    //         localStorage.setItem("selectedProduct", "Movie")
            
            
    //     }
    //     else if(selectedProduct == "Games"){
    //         $("#movieType").hide();
    //         $("#gameType").show();
    //         localStorage.setItem("selectedProduct", "Game")
    //     }
    //     else{
    //         $("#movieType").hide();
    //         $("#gameType").hide();
    //     }

    // });

    // $("#submitNewItem").on("click", function(){
    //     $("#jsonObjectText").html("");
    //     $("#productUPC").html("");
    //     $("#ProductType").html("");
    //     $("#productTitle").html("");
    //     $("#ProductKind").html("");

    //     var upcString = $("#UPC").val();
    //     var productTypeString = $(".productTypeList").children("option:selected").val();
    //     var productNameString = $("#title").val();
    //     var productAttrString;
        
    //     //var selectedProduct = localStorage.getItem("selectedProduct")
         
    //     if(localStorage.getItem("selectedProduct") == "Movie"){
    //         productAttrString = $("#moveTypeList").children("option:selected").val();
    //     }else if(localStorage.getItem("selectedProduct") == "Game"){
    //         productAttrString = $("#gameTypeList").children("option:selected").val();
    //     }
        
    //     if(upcString == "" || productTypeString == "" || productNameString == "" || productAttrString == ""){
    //         $("#jsonObjectText").html("YOU MUST FILL OUT ALL OF THE INFORMATION TO ADD!");
    //     }
    //     else{
    //         $("#jsonObjectText").html("");
    //         $("#productUPC").html("");
    //         $("#ProductType").html("");
    //         $("#productTitle").html("");
    //         $("#ProductKind").html("");
    //         var isFound = checkForDuplicate($("#UPC").val());
    //         if(isFound == false){
    //             $("#jsonObjectText").html("");
    //             saveNewEntry();
    //         }
    //         else{

    //             var myArray =JSON.parse(localStorage.getItem("product_info"));

    //             console.log("The upc is:" +  myArray[0]);

    //             $("#productUPC").html("UPC: " + myArray[0]);
    //             $("#ProductType").html("TYPE OF PRODUCT: : " + myArray[1]);
    //             $("#productTitle").html("TITLE: " + myArray[2]);
    //             $("#ProductKind").html("PRODUCT SPEC: " + myArray[3]);

    //             //resetAddItemForm();
    //             $("#jsonObjectText").html("YOU ALREADY OWN THIS PRODUCT!");

    //             resetAddItemForm();
    //         }
    //     }
    // });

}

function checkForDuplicate(upcValue){
    //alert(upcValue)
    if(localStorage.getItem("selectedProduct") == "Movie"){
        if(JSONObject[upcValue] == "" || JSONObject[upcValue] == null){
            return false;
        }
        else{
            var array = [upcValue, JSONObject[upcValue].productType, JSONObject[upcValue].productName, JSONObject[upcValue].productAttr];

            localStorage.setItem("product_info", JSON.stringify(array));

            return true;
        }

    }else if(localStorage.getItem("selectedProduct") == "Game"){
        if(JSONObjectGame[upcValue] == "" || JSONObjectGame[upcValue] == null){
            return false;
        }
        else{
            console.log(upcValue);

            var array = [upcValue, JSONObjectGame[upcValue].productType, JSONObjectGame[upcValue].productName, JSONObjectGame[upcValue].productAttr];

            localStorage.setItem("product_info", JSON.stringify(array));

            return true;

        }
    }
}

function saveNewEntry(){
    // $("#productUPC").html("");
    // $("#ProductType").html("");
    // $("#productTitle").html("");
    // $("#ProductKind").html("");

    var upcString = $("#UPC").val();
    var productTypeString = $(".productTypeList").children("option:selected").val();
    var productNameString = $("#title").val();
    var productAttrString;
    
    //var selectedProduct = localStorage.getItem("selectedProduct")
     
    if(localStorage.getItem("selectedProduct") == "Movie"){
        productAttrString = $("#moveTypeList").children("option:selected").val();
    }else if(localStorage.getItem("selectedProduct") == "Game"){
        productAttrString = $("#gameTypeList").children("option:selected").val();
    }

    if(localStorage.getItem("selectedProduct") == "Movie"){
        JSONObject[upcString] = new Object();
        JSONObject[upcString].productType = productTypeString;
        JSONObject[upcString].productName = productNameString
        JSONObject[upcString].productAttr = productAttrString;

        localStorage.setItem("JSONObject", JSON.stringify(JSONObject))

        //$("#jsonObjectText").html(JSONObject);
    }else if(localStorage.getItem("selectedProduct") == "Game"){
        JSONObjectGame[upcString] = new Object();
        JSONObjectGame[upcString].productType = productTypeString;
        JSONObjectGame[upcString].productName = productNameString;
        JSONObjectGame[upcString].productAttr = productAttrString;

        localStorage.setItem("JSONObjectGame", JSON.stringify(JSONObjectGame))

        //$("#jsonObjectText").html(JSONObjectGame);
    }

    $("#jsonObjectText").html("YOUR PRODUCT HAS BEEN SAVED!");

    resetAddItemForm();
    
}

function resetAddItemForm(){
    // $('[name = productTypeList]').val( '' );
    // $('[name = moveTypeList]').val( '' );
    // $('[name = gameTypeList]').val( '' );

    // $("#gameType").hide();
    // $("#movieType").hide();

    $("#UPC").val("");
    $("#title").val("");
}

function resetAllAddFormItems(){
    $('[name = productTypeList]').val( '' );
    $('[name = moveTypeList]').val( '' );
    $('[name = gameTypeList]').val( '' );

    $("#gameType").hide();
    $("#movieType").hide();

    $("#UPC").val("");
    $("#title").val("");
}

function resetScanUPCForm(){
  

    $("#checkForUPCDiv").hide();
    $("#getUPCForCheck").hide();

    $('[name = productTypeListUPCCheck]').val( '' );
    $("#textCheckUPC").val("");

}

function resetShowItemsDiv(){
    $("#showItemsDiv").hide();
    $("#showMovieType").hide();
    $("#showgameType").hide();
    $('[name = showmoveTypeList]').val( '' );
    $('[name = showgameTypeList]').val( '' );
    $('[name = showItemsList]').val( '' );

}

function getItemsData(){
    var productTypeString = $(".showItemsList").children("option:selected").val();
    var productAttrString;
    
    //var selectedProduct = localStorage.getItem("selectedProduct")
     
    if(localStorage.getItem("selectedProduct") == "Movie"){
        productAttrString = $("#showmoveTypeList").children("option:selected").val();
    }else if(localStorage.getItem("selectedProduct") == "Game"){
        productAttrString = $("#showgameTypeList").children("option:selected").val();
    }

    // console.log(productType);
    // console.log(productAttr);
    if(localStorage.getItem("selectedProduct") == "Movie"){
        for(key in JSONObject){
            if(productAttrString == "all"){
                var showproductType = JSONObject[key].productType;
                var showProductName = JSONObject[key].productName;
                var showProductAttr = JSONObject[key].productAttr;

                var newObject = new Object();
                newObject.upc = key;
                newObject.type = showproductType;
                newObject.name = showProductName;
                newObject.attr = showProductAttr;
                sortedArray.push(newObject)
            }
            else{
                if(JSONObject[key].productAttr == productAttrString){
                    var showproductType = JSONObject[key].productType;
                    var showProductName = JSONObject[key].productName;
                    var showProductAttr = JSONObject[key].productAttr;
    
                    var newObject = new Object();
                    newObject.upc = key;
                    newObject.type = showproductType;
                    newObject.name = showProductName;
                    newObject.attr = showProductAttr;
                    sortedArray.push(newObject)
                }  
            }
          
        }
    }
    else{
        for(key in JSONObjectGame){
            if(productAttrString == "all"){
                var showproductType = JSONObjectGame[key].productType;
                var showProductName = JSONObjectGame[key].productName;
                var showProductAttr = JSONObjectGame[key].productAttr;

                var newObject = new Object();
                newObject.upc = key;
                newObject.type = showproductType;
                newObject.name = showProductName;
                newObject.attr = showProductAttr;
                sortedArray.push(newObject)
            }
            else{
                if(JSONObjectGame[key].productAttr == productAttrString){
                    var showproductType = JSONObjectGame[key].productType;
                    var showProductName = JSONObjectGame[key].productName;
                    var showProductAttr = JSONObjectGame[key].productAttr;
    
                    var newObject = new Object();
                    newObject.upc = key;
                    newObject.type = showproductType;
                    newObject.name = showProductName;
                    newObject.attr = showProductAttr;
                    sortedArray.push(newObject)
                }  
            }
          
        }

    }

    console.log(sortedArray);
    sortData();

}

function sortData(){
    sortedArray.sort(dynamicSort("name"));
    //console.log(sortedArray);
    displayItems();
}

function displayItems(){
    var tableInfo = "<tr style='background-color:	#f00000;'><th>UPC</th>";
    // tableInfo += "<th>Pokemon Number</th>";
    tableInfo += "<th>Title</th>";
    tableInfo += "<th>Product Type</th>";
    tableInfo += "<th></th></tr>";

    for(var x = 0; x < sortedArray.length; x++){
        tableInfo += "<tr><td class='productRow'  id='" + sortedArray[x].upc +"'>"+ sortedArray[x].upc + "</td>" 
        tableInfo += "<td class='productRow'  id='" + sortedArray[x].name +"'>"+ sortedArray[x].name.toUpperCase() + "</td>" 
        tableInfo += "<td class='productRow'  id='" + sortedArray[x].type +"'>"+ sortedArray[x].type.toUpperCase() + "</td>" 
        tableInfo += "<td class='productRow'  id='" + sortedArray[x].attr +"'>"+ sortedArray[x].attr.toUpperCase() + "</td></tr>" 
    }

    $("#showItemTable").append(tableInfo);
    
}

function dynamicSort(property) {
    var sortOrder = 1;

    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }

    return function (a,b) {
        if(sortOrder == -1){
            return b[property].localeCompare(a[property]);
        }else{
            return a[property].localeCompare(b[property]);
        }        
    }
}